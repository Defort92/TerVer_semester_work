import pandas


def read_xml(file, page, name):
    result = []
    try:
        result = pandas.read_excel(file, sheet_name=page)
        print(result)
        result = result[name].tolist()
    except FileNotFoundError as e:
        print('Файл невозможно прочитать')
    return result


if __name__ == "__main__":
    print(read_xml("C:/Users/mrkaz/Desktop/dota2Train.xlsx", "dota2Train", 1))
