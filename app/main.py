from math_class import Statistics
from reader import read_xml


def func(file, page, name, alpha: float = 0.05):
    data = read_xml(file, page, name)
    math = Statistics(data, alpha)
    table = []
    y_list = []
    x_list = []
    for row in math.table:
        row["interval"][0] = round(row["interval"][0], 3)
        row["interval"][1] = round(row["interval"][1], 3)
        y_list.append(row["interval_frequency"])
        x_list.append(row["x_average"])
        list_1 = list(row.values())
        list_2 = []
        for i in list_1:
            if isinstance(i, float):
                list_2.append(round(i, 3))
            else:
                list_2.append(i)
        table.append(list_2)

    return (f'Количество элементов: {math.amount}.\n\n'
            f'Минимальный лемент: {math.min}.\n\n'
            f'Максимальный элемент: {math.max}.\n\n'
            f'Количество интервалов: {math.interval_amount}.\n\n'
            f'Длина интервала: {math.interval_length}.\n\n'
            f'Среднее значение: {math.average_value}.\n\n'
            f'Дисперсия: {math.dispersion}.\n\n'
            f'Отклонение: {math.root_of_dispersion}.\n\n'
            f'Дов. инт. для значения: {math.confidence_interval_of_average_to_string}.\n\n'
            f'Дов. инт. для дисперсии: {math.confidence_interval_of_dispersion_to_string}.\n\n'
            f'Мода: {math.fashion}.\n\n'
            f'Медиана: {math.median}.\n\n'
            f'M3: {math.m_third}.\n\n'
            f'M4: {math.m_fourth}.\n\n'
            f'A3: {math.asymmetry}.\n\n'
            f'Эксцесс: {math.excess}.\n'), table, math.interval_length, x_list, y_list


if __name__ == "__main__":
    print(func("C:/Users/mrkaz/Desktop/dota2Train.xlsx", "dota2Train", 1))
