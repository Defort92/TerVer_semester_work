from scipy.stats import chi2
from scipy.stats import t as student
import math


class Statistics:

    def __init__(self, list_, alpha: float = 0.05):
        self.list = list_
        self.list.sort()
        self.amount = len(list_)
        self.min = self.search_min()
        self.max = self.search_max()
        self.interval_amount = self.count_interval_amount()
        self.interval_length = self.count_interval_length()
        self.average_value = 0.0
        self.table = self.create_table()
        self.fill_table()
        self.dispersion = self.count_dispersion()
        self.root_of_dispersion = self.count_root_of_dispersion()
        self.fashion = self.count_fashion()
        self.median = self.count_median()
        self.m_third = self.count_m_third()
        self.m_fourth = self.count_m_fourth()
        self.asymmetry = self.count_a_third()
        self.excess = self.count_excess()
        self.alpha = alpha
        self.confidence_interval_of_average = self._confidence_interval_of_average()
        self.confidence_interval_of_dispersion = self._confidence_interval_of_dispersion()

    def search_min(self):
        min_ = self.list[0]
        return min_

    def search_max(self):
        max_ = self.list[-1]
        return max_

    def count_interval_amount(self):
        count_int = 1 + 3.322*math.log(self.amount, 10)
        return int(round(count_int))

    def count_interval_length(self):
        residual = self.max - self.min
        length = residual / self.interval_amount + 0.00000000000001
        return length

    def count_interval(self):
        interval = self.min
        for table in self.table:
            table["interval"] = [interval, interval + self.interval_length]
            interval += self.interval_length

    def count_frequency(self):
        for num in self.list:
            for table in self.table:
                if table["interval"][0] <= num < table["interval"][1]:
                    table["frequency"] += 1

    def count_frequency_funded(self):
        fund = 0
        for table in self.table:
            fund += table["frequency"]
            table["frequency_funded"] = fund

    def count_interval_frequency(self):
        for table in self.table:
            table["interval_frequency"] = table["frequency"] / self.amount

    def count_x_average(self):
        for table in self.table:
            table["x_average"] = table["interval"][0] + self.interval_length / 2

    def count_average_value(self):
        result = 0
        for table in self.table:
            result += table["x_average"] * table["frequency"] / self.amount
        return result

    def count_z(self):
        for table in self.table:
            table["z"] = table["x_average"] - self.average_value
            table["z_squared"] = table["z"] ** 2
            table["z_third"] = table["z"] ** 3
            table["z_fourth"] = table["z"] ** 4

    def create_table(self):
        table = []
        for i in range(self.interval_amount):
            table.append({
                'interval': [0, 0],
                'frequency': 0,
                'frequency_funded': 0,
                'interval_frequency': 0.0,
                'x_average': 0.0,
                'z': 0.0,
                'z_squared': 0.0,
                'z_third': 0.0,
                'z_fourth': 0.0
            })
        return table

    def fill_table(self):
        self.count_interval()
        self.count_frequency()
        self.count_frequency_funded()
        self.count_interval_frequency()
        self.count_x_average()
        self.average_value = self.count_average_value()
        self.count_z()

    def count_dispersion(self):
        dispersion = 0
        for table in self.table:
            dispersion += (table['frequency'] * table['z_squared']) / self.amount
        return dispersion

    def count_root_of_dispersion(self):
        root = math.sqrt(self.dispersion)
        return root

    def search_fashion_line(self):
        frequency = 0
        index_of_interval = 0
        for i in range(self.interval_amount):
            if self.table[i]['frequency'] >= frequency:
                frequency = self.table[i]['frequency']
                index_of_interval = i
        return index_of_interval

    def count_fashion(self):
        index = self.search_fashion_line()
        x_0 = self.table[index]["interval"][0]
        n_m = self.table[index]["frequency"]
        n_m_minus_1 = self.table[index - 1]['frequency']
        n_m_plus_1 = self.table[index + 1]['frequency']
        fashion = x_0 + ((n_m - n_m_minus_1) * self.interval_length/((n_m - n_m_minus_1) + (n_m - n_m_plus_1)))
        return fashion

    def search_median_line(self):
        marker = self.list[self.amount // 2]
        for i in range(self.interval_amount):
            if self.table[i]["interval"][0] <= marker < self.table[i]["interval"][1]:
                return i

    def count_median(self):
        index = self.search_median_line()
        x_0 = self.table[index]["interval"][0]
        n = 0.5 * self.amount
        n_m_minus_one = self.table[index - 1]["frequency_funded"]
        n_m = self.table[index]["frequency"]
        median = x_0 + ((n - n_m_minus_one) * self.interval_length / n_m)
        return median

    def count_m_third(self):
        m_third = 0
        for table in self.table:
            m_third += (table["frequency"] * table["z_third"]) / self.amount
        return m_third

    def count_m_fourth(self):
        m_fourth = 0
        for table in self.table:
            m_fourth += (table["frequency"] * table["z_fourth"]) / self.amount
        return m_fourth

    def count_a_third(self):
        dispersion_third = self.root_of_dispersion ** 3
        a_third = self.m_third / dispersion_third
        return a_third

    def count_excess(self):
        dispersion_fourth = self.root_of_dispersion ** 4
        e_k = (self.m_fourth / dispersion_fourth) - 3
        return e_k

    def _confidence_interval_of_average(self):
        safety_factor = student.ppf((1 + self.alpha) / 2, self.amount - 1)
        print("safety_factor -- ", safety_factor)
        step = safety_factor * self.root_of_dispersion / math.sqrt(self.amount)
        print("step -- ", step)
        return self.average_value - step, self.average_value + step

    def _confidence_interval_of_dispersion(self):
        chi_square_first = chi2.ppf(1 - self.alpha / 2, self.amount - 1)
        chi_square_second = chi2.ppf(self.alpha / 2, self.amount - 1)
        numerator = (self.amount - 1) * self.dispersion
        return numerator / chi_square_first, numerator / chi_square_second

    @property
    def confidence_interval_of_average_to_string(self):
        return self.confidence_interval_of_average

    @property
    def confidence_interval_of_dispersion_to_string(self):
        return self.confidence_interval_of_dispersion
