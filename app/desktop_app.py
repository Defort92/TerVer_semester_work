from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from main import func
import tkinter as tk
import PySimpleGUI as sg


def create_desktop_app():
    sg.theme("DarkBlue14")

    file_reader_column = [
        [
            sg.Text("Выберите файл:         "),
            sg.In(size=(25, 1), enable_events=True, key="-FILE-"),
            sg.FileBrowse(button_text="Искать", file_types=(("Text Files", "*.x*"),)),
        ],
        [
            sg.Text("Название страницы:   "),
            sg.In(size=(25, 1), enable_events=True, key="-SHEET-"),
        ],
        [
            sg.Text("Название столбца:     "),
            sg.In(size=(25, 1), enable_events=True, key="-COLUMN-"),
        ],
        [
            sg.Text("Уровень значимости: "),
            sg.In(size=(25, 1), enable_events=True, key="-ALPHA-", default_text="0.05"),
        ],
        [
            sg.Button("Посчитать")
        ],
        [
            sg.Table(values=[], headings=["Интервал", "Частота", "Накоп. частота", "Частота интервала",
                                          "Х ср", "  Z  ", " Z^2 ", " Z^3 ", " Z^4 "],
                     col_widths=[10, 5, 5, 5, 5, 6, 6, 6, 6],
                     auto_size_columns=False,
                     display_row_numbers=True,
                     alternating_row_color="Midnight blue",
                     justification='center',
                     num_rows=10,
                     key='-TABLE-',
                     row_height=35,
                     tooltip='This is a table',
                     pad=(0, 50))
        ],
    ]

    statistic_column = [
        [
            sg.Text(f'Количество элементов:\n\n'
                    f'Минимальный лемент:\n\n'
                    f'Максимальный элемент:\n\n'
                    f'Количество интервалов:\n\n'
                    f'Длина интервала:\n\n'
                    f'Среднее значение:\n\n'
                    f'Дисперсия:\n\n'
                    f'Отклонение:\n\n'
                    f'Дов. инт. для значения:\n\n'
                    f'Дов. инт. для дисперсии:\n\n'
                    f'Мода:\n\n'
                    f'Медиана:\n\n'
                    f'M3:\n\n'
                    f'M4:\n\n'
                    f'A3:\n\n'
                    f'Эксцесс:\n', size=(40, 300), key="-INFO-", justification="left", pad=(0, 40))
        ]
    ]

    histogram_column = [
        [
            sg.Canvas(size=(300, 300), background_color='red', key="-CANVAS-"),
        ]
    ]

    layout = [
        [
            sg.Column(file_reader_column),
            sg.Column(statistic_column),
            sg.Column(histogram_column)
        ]
    ]

    window = sg.Window('My File Browser', layout, size=(1280, 600), finalize=True)

    figure = Figure(figsize=(6, 6), dpi=60)
    subplot = figure.add_subplot(111)
    rectangles = subplot.bar([0], [0], 1)
    canvas = FigureCanvasTkAgg(figure, master=window["-CANVAS-"].TKCanvas)
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.LEFT)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == "Exit":
            break
        elif event == "Посчитать":
            try:
                print("-FILE-", values["-FILE-"])
                print("-SHEET-", values["-SHEET-"])
                print("-COLUMN-", values["-COLUMN-"])
                values["-ALPHA-"] = float(values["-ALPHA-"])
                info, table, length, x_list, y_list = func(
                    values["-FILE-"], values["-SHEET-"], int(values["-COLUMN-"]), values["-ALPHA-"]
                )
                window["-INFO-"].update(info)
                window["-TABLE-"].update(values=table, num_rows=len(table))

                subplot.clear()
                canvas.get_tk_widget().delete(canvas.get_tk_widget().find_all())
                rectangles = subplot.bar(x_list, y_list, length)
                # canvas = FigureCanvasTkAgg(figure, master=window["-CANVAS-"].TKCanvas)  # мой кусок кода
                canvas.draw()
                canvas.get_tk_widget().pack(side=tk.RIGHT)
            except Exception as e:
                sg.Popup('Произошла ошибка, укажите верный путь до данных', keep_on_top=True)


create_desktop_app()
